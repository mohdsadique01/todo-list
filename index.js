const express=require('express')
const port=8000;
const fs=require('fs')
const app=express()
const cors=require('cors')
const bodyParser=require('body-parser')
app.use(cors())
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
app.get('/',(req,res)=>{
    res.send('hi dear')
})
app.post('/send',(req,res)=>{
    const data=req.body
    console.log(data);
    let db=JSON.parse(fs.readFileSync('./database.json','utf-8'))
    db.push(data)
    fs.writeFileSync('./database.json',JSON.stringify(db))
    res.send('data created')
})
app.get('/getdb',(req,res)=>{
    let db=JSON.parse(fs.readFileSync('./database.json','utf-8'))
    console.log(db);
    res.send(db)

})
app.post('/del',(req,res)=>{
    const idx=req.body.one
    console.log(idx);
    // console.log(data);
    let db=JSON.parse(fs.readFileSync('./database.json','utf-8'))
    db.splice(idx,1)
    fs.writeFileSync('./database.json',JSON.stringify(db))
    res.send('data created')
})


app.listen(port,(e)=>{
    console.log('server ready');
})